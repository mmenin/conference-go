from .keys import pexel_api_key, open_weather_api_key
import requests

from rich import print

def get_picutre_url(city:str,state:str):
    """
    Uses PEXEL API to get picture in query. (input is query)
    Returns URL string.
    """
    url = f"https://api.pexels.com/v1/search?query={ city }+{ state }&size=small&per_page=1"
    headers = {'Authorization': pexel_api_key}
    r = requests.get(url,headers=headers)
    pexel_data = r.json()
    if pexel_data.get("cod") != 200: # if not request 200 OK
        return None
    else:
        return pexel_data.get("photos")[0]["url"]    # NULL ??

def get_weather_data(city:str,state:str):
    """
    Uses WEATHER API to get location temperature and weather conditions.  Returns TEMP and DESCRIPTION.
    """
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={ city },{ state },US&limit=1&appid={open_weather_api_key}"
    r = requests.get(url)
    loc_data = r.json()
    print("Debug ===================================== 1")
    lat,lon = loc_data[0].get("lat"),loc_data[0].get("lon")
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={open_weather_api_key}&units=imperial"
    r = requests.get(url)
    w_data = r.json()
    if w_data.get("cod") != 200: # if not request 200 OK
        return None
    else:
        temp,desc = w_data.get("main").get("temp"),w_data.get("weather")[0].get("description")
        return {"temp":temp,"description":desc}